const axios = require('axios');

module.exports =  {
    wakeUp: () => {
        axios.post("https://excel-export-service.herokuapp.com/api/excel", [{'hi': "hi"}]);
    },

    export: (data, options = { fileName: "excel", headings: null }) => {
        return new Promise((res, rej) => {
            if (options.headings && options.headings.length > 0) {
                axios.post("https://excel-export-service.herokuapp.com/api/excel/headings", {
                    "headings": options.headings,
                    "data": data
                }).then((response) => {
                    window.open(`https://excel-export-service.herokuapp.com/api/excel?excelId=${response.data}&excelName=${options.fileName}`);
                    res();
                }).catch(error => {
                    rej(error);
                })
            } else {
                axios.post("https://excel-export-service.herokuapp.com/api/excel", data).then((response) => {
                    window.open(`https://excel-export-service.herokuapp.com/api/excel?excelId=${response.data}&excelName=${options.fileName}`);
                    res();
                }).catch(error => {
                    rej(error)
                });
            }
        })
    },
}