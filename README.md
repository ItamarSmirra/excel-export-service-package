# Excel Export Service Package
An easy to use package to export your data into nice xls file.<br>
Be aware that this package send axios requests to an external server so you must be connected to the net, and it might take few seconds.

# Install
`npm install export-to-excel.js`<br>

# Usage
First of all you need to wake up the external server, like this:<br>
```javascript
import ExcelService from 'export-to-excel.js';
ExcelService.wakeUp();
```

Put this code immediately after your application is up (it takes faw seconds to wake up the external server).

**Simple export to excel**

```javascript
import ExcelService from 'export-to-excel.js';
const data = [{ "id": 5, "name": "John", "grade": 90, "age": 15 }, { "id": 7, "name": "Nick", "grade": 70, "age": 17 }];
ExcelService.export(data);
```

The excel file that will be downloaded looks like -
<br>
![alt text](https://gitlab.com/ItamarSmirra/excel-export-service-package/raw/master/NoHeadings.png)

**Excel with headings** <br>
If you want to control the excel headings you can do it easily by sending list of headings to the function:<br>
```javascript
import ExcelService from 'export-to-excel.js';
const data = [{ "id": 5, "name": "John", "grade": 90, "age": 15 }, { "id": 7, "name": "Nick", "grade": 70, "age": 17 }];
const headings = ["Student ID", "Student Name", "Test Grade", "Student Age"];
ExcelService.export(data, { headings });
```

The excel file that will be downloaded looks like -
<br>
![alt text](https://gitlab.com/ItamarSmirra/excel-export-service-package/raw/master/WithHeadings.png)

**Name the file** <br>
Currently the excel file name is 'excel'.<br>
You can override it:
```javascript
const data = [{ "id": 5, "name": "John", "grade": 90, "age": 15 }, { "id": 7, "name": "Nick", "grade": 70, "age": 17 }];
const headings = ["Student ID", "Student Name", "Test Grade", "Student Age"];
const fileName = 'StudentsGrades';
ExcelService.export(data, { headings, fileName });
```

# Async/Await
The method 'export' is promise based, so you can use it with async/await syntax:
```javascript
const onExport = async () => {
    await ExcelService.export(this.data, { fileName: this.excelName });
    alert('Your excel file has been exported!'); // or some nicer ui indication...
}
```

# Options

| option | description | default value |
| ------ | ------ | ------ |
| fileName | the name of the excel file to be exported | excel |
| headings | the headings for the excel file, if not presented the headings will be the object keys | - |

<br>

## License

[MIT](LICENSE)